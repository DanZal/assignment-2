
//balance texts and stuff
const payElement = document.getElementById("pay");
const balanceElement = document.getElementById("balance");
const outstandingLoanElement = document.getElementById("outstandingLoan");

const computerElement = document.getElementById("computers");
const featureElement = document.getElementById("featureLaptop");

const computerTitleElement = document.getElementById("computerTitle");
const computerPriceElement = document.getElementById("computerPrice");
const computerDescriptionElement = document.getElementById("computerDescription");
const computerImageElement = document.getElementById("photo");

const buyElement = document.getElementById("buySecond");



const workButtonElement = document.getElementById("workBtn");

//Testing buttons on the top that work
const payButtonElement = document.getElementById("bankSecond");
const addElement = document.getElementById("workSecond");
const loanElement = document.getElementById("loanSecond");
const repayLoanElement = document.getElementById("repayLoanSecond");

let workBalance = 0.0;
let totalBankBalance = 0.0;
let loanBalance = 0.0;

let hasLoan = false;
let hasBoughtComputer = false;

let pcs = [];


const handleWork = () => {
    workBalance += 100;
    payElement.innerText = `Pay: ${workBalance}kr`
}

const handleBank = () => {

        //Need to make sure that loanblance does not equal a negative nr
        if (hasLoan === true && loanBalance > 0) {
            totalBankBalance += (workBalance*0.9);
            if (workBalance *0.1 > loanBalance) {
                let restAmount = workBalance*0.1 - loanBalance;
                workBalance = restAmount;
                loanBalance = 0;
                repayLoanElement.style.display = 'none';
                outstandingLoanElement.innerText = `Outstanding Loan is: ${loanBalance}`;
                payElement.innerText = `Pay: ${workBalance}kr`
                balanceElement.innerText = `Bank balance: ${totalBankBalance}kr`
                hasLoan = false;
            } else {
                loanBalance -= (workBalance *0.1);
                outstandingLoanElement.innerText = `Outstanding Loan is: ${loanBalance}`;
                workBalance = 0;
                payElement.innerText = `Pay: ${workBalance}kr`
                balanceElement.innerText = `Bank balance: ${totalBankBalance}kr`
            }
         
        } else {
        
        outstandingLoanElement.innerText = "";
        totalBankBalance += workBalance;
        workBalance = 0;
        payElement.innerText = `Pay: ${workBalance}kr`
        balanceElement.innerText = `Bank balance: ${totalBankBalance}kr`
        hasLoan = false;
        }


    } 
 



const handleLoan = () => {
        if (hasLoan === false || loanBalance <= 0) {
            console.log("hasLoan is false")
            const totalLoan = prompt("Please enter the amount of money you wish to loan: ");
            //Input null term func
            if (totalLoan === null) {
                return;
            }
            //Input validation
            if (isNaN(totalLoan) || totalLoan === "") {
                alert("Invalid input please enter a numerical value");
                return false;
            }

            if ( totalLoan > totalBankBalance*2) {
                alert(`You cannot loan twice your bank balance. Desired loan amount is: ${totalLoan}, whereas bank balance is: ${totalBankBalance}`);
            } else if (loanBalance > 0 && hasBoughtComputer === false) {
                alert("You need to buy a computer before aquiring a new loan")
            }
            else {
                alert(`You have loaned: ${totalLoan}`);
                loanBalance = totalLoan;
                totalBankBalance += parseFloat(totalLoan);
                outstandingLoanElement.innerText = `Outstanding Loan is: ${loanBalance}`;
                balanceElement.innerText = `Bank balance: ${totalBankBalance}kr`;
                hasLoan = true;
                hasBoughtComputer = false;
                repayLoanElement.style.display = "inline-block";

            }
        } else {
            alert(`You already have a loan`)
        }


}
//todo:if workBalance is more than loanbalnce. take workBalnce - loanbalance. add to to the loanbalacne and add rest to bank or workbalance
const handleRepay = () => {
    
    console.log("repay button clciked")
    if (loanBalance > 0) {

        if (workBalance > loanBalance) {
            let restAmount = workBalance - loanBalance;
            loanBalance = 0;
            workBalance = restAmount;
            payElement.innerText = `Pay: ${workBalance}kr`
            outstandingLoanElement.innerText = `Outstanding Loan is: ${loanBalance}`
            hasLoan=false;
            repayLoanElement.style.display = 'none';

        } else {
            loanBalance -= (workBalance);
            workBalance = 0;
            payElement.innerText = `Pay: ${workBalance}kr`
            outstandingLoanElement.innerText = `Outstanding Loan is: ${loanBalance}`
        }
       
    }
 }

 const handleBuy = () => {

    const selectedPc = pcs[computerElement.selectedIndex];

    if (totalBankBalance < selectedPc.price) {
        alert("You don't have enough in your bank balance to buy this computer")
    } else {
        totalBankBalance -= selectedPc.price;
        balanceElement.innerText = `Bank balance: ${totalBankBalance}kr`;
        alert("Congrats you bought a new computer!")
        hasBoughtComputer = true;
    }

 }


//test func for button clicks
const testBtnClick = () => {
    console.log("btn clciked")
}



repayLoanElement.addEventListener("click", handleRepay)
addElement.addEventListener("click", handleWork);
loanElement.addEventListener("click", handleLoan)

if(payButtonElement) {
    payButtonElement.addEventListener("click", handleBank);
}



fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    //Setting our pcs array to data we get from the json resposne
    .then(data => pcs = data)
    .then(pcs => addPcsToMenu(pcs)); 

//Method to add each pc in pcs array
const addPcsToMenu = (pcs) => {
    pcs.forEach(pc => addPcToMenu(pc));
   //loads the price of the first pc since we are using onChange listener to update the price
    featureElement.innerText = pcs[0].specs;
    computerTitleElement.innerText = pcs[0].title;
    computerPriceElement.innerText = pcs[0].price  + " NOK";
    computerDescriptionElement.innerText = (pcs[0].description);
    computerImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/"+pcs[0].image;
}

const addPcToMenu = (pc) => {
    const pcElement = document.createElement("option");
    pcElement.value = pc.id;
    pcElement.appendChild(document.createTextNode(pc.title));
    computerElement.appendChild(pcElement);
}

//runs whenever we change pc
const handleDrinkMenuChange = () => {
    //gets the information (price) of the selected pc
    const selectedPc = pcs[computerElement.selectedIndex];
    //priceElement in the HTML's text is changed to the selected pc price
    featureElement.innerText = selectedPc.specs;
    computerTitleElement.innerText = selectedPc.title;
    computerPriceElement.innerText = selectedPc.price  + " NOK";
    computerDescriptionElement.innerText = (selectedPc.description);
    computerImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/"+selectedPc.image;


}

buyElement.addEventListener("click", handleBuy);
computerElement.addEventListener("change", handleDrinkMenuChange);
